import smtplib
from email.message import EmailMessage
from nameko.rpc import rpc

def send_email(otp, email):
    msg = EmailMessage()
    text = "OTP สำหรับการเปลี่ยนรหัสผ่านของท่าน คือ " + str(otp) + ", OTP ของท่านจะหมดอายุใน 2 นาที"
    msg.set_content(text)

    msg['Subject'] = 'Change Password OTP'
    msg['To'] = email

    s = smtplib.SMTP("smtp_otp",25)
    s.ehlo()
    s.sendmail(from_addr = 'anhnguyenh64@nu.ac.th', to_addrs = email, msg = msg.as_string())
    s.quit()


class Email:
    name = "email_otp"

    @rpc
    def send(self, otp, email):
        send_email(otp, email)